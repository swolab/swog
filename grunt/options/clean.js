module.exports = {
	dist: {
		src: [
			"<%=pkg.distdir%>/css",
			"<%=pkg.distdir%>/js"
		]
	},
	swogcss: {
		src: [
			"<%=pkg.distdir%>/css/swog*"
		]
	},
	bscss: {
		src: [
			"<%=pkg.distdir%>/css/bootstrap*"
		]
	},
	bsjs: {
		src: [
			"<%=pkg.distdir%>/js/bootstrap*"
		]
	},
	swogjs: {
		src: [
			"<%=pkg.distdir%>/js/swog*"
		]
	},
};
